<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class Traumatologias extends Model
{
    protected $table='tb_traumatologias';
    protected $primaryKey='tra_id';
    public $timestamps=false;

    protected $fillable=[
    		'tra_cant',
    		'tra_causas',
    		'tra_fecha',
    ];

    protected $guarded=[
    ];
}
