<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class MorbiEspecialidad extends Model
{
    protected $table= 'tb_morbilidadEspecialidades';
    protected $primaryKey= 'mde_id';
    public $timestamps=false;


    protected $fillable = [
    	'mde_causas',
    	'mde_especialidad',
    	'mde_fecha'
    ];

    protected $guarded = [

    ];
}
