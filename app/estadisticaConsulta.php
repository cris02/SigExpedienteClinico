<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class estadisticaConsulta extends Model
{
    //
    protected $table='tb_estadisticaconsultas';
    protected $primaryKey='est_id';
    public $timestamps=false;

    protected $fillable=[
    		'est_nombreMedico',
    		'est_especialidad',
    		'est_fecha',
    		'est_cant'
    		];

    protected $guarded=[
    
    ];
}
