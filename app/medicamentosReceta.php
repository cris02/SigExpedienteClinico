<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class medicamentosReceta extends Model
{
    //Hace referencia a la tabla medicamentoRecetado
    protected $table='medicamentoRecetado';

    //Hacer referencia a la llave primaria
    protected $primaryKey = "mre_id";

    public $timestamps=false;

    protected $fillable = [
    	'mre_nombre',
    	'mre_fecha',
        'mre_cant'
    ];

    protected $guarded=[

    ];
}
