<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class bitacora extends Model
{
    //
    protected $table = 'tb_bitacoras';
    protected $primaryKey = 'bit_id';
    public $timestamps=false;

    protected $fillable = [
        'bit_id',
        'usu_id',
    	'bit_nombreUsuario',
    	'bit_fecha'
    ];

    protected $guarded=[

    ];


}
