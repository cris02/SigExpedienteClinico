<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
     protected $table='tb_expedientes';
    protected $primaryKey='exp_id';
    public $timestamps=false;

    protected $fillable=[
    		'cant',
    		'exp_sexo',
    		'exp_fecha',
    		];

    protected $guarded=[
    ];
}
