<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    protected $table = 'tb_referencias';
    protected $primaryKey = 'ref_id';	
    	
    public $timestamps = false;

    protected $fillable = [
		'id',
		'ref_causa',
		'ref_especialidad',
		'ref_fecha'
    ];
}
