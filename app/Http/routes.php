<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('medico');
});

Route::resource('consulta','consultaController');
Route::resource('consultaT','consultaTController');

Route::resource('medicamentoRecetado','medicamentoRecetaController');
Route::resource('medicamentoRecetadoT','medicamentoRecetaTController');

Route::resource('estadisticaConsulta', 'estadisticaConsultaController');
Route::resource('estadisticaConsultaT', 'estadisticaConsultaTController');

Route::resource('bitacora', 'bitacoraController');


Route::resource('medEspecialidad','medEspecialidadController');

Route::resource('morbiEspecialidad','morbiEspecialidadController');

Route::resource('cuadroMedicamentos','cuadroMedicamentosController');
Route::resource('cuadroConsultas','cuadroConsultasController');

Route::resource('traumatologia','TraumatologiaController');
Route::resource('traumatologiaT','TraumatologiaTController');

Route::resource('morbilidad','MorbilidadController');
Route::resource('morbilidadT','MorbilidadTController');

Route::resource('expediente','ExpedienteController');

Route::resource('referencia', 'ReferenciaController');
Route::resource('referenciaT','ReferenciaTController');

Route::post('/test/save',['as'=>'save-date','uses' => 'cuadroMedicamentosController@index',function () {return '';}]);


//rutas gestionar PDF
  
 Route:: get('download_medEsp','PdfController@download_medEsp');
 
 Route:: get('download_morbi','PdfController@download_morbi');

 Route:: get('download_cuadroConsultas','PdfController@download_cuadroConsultas');

 Route:: get('download_cuadroMed','PdfController@download_cuadroMed');

 Route:: get('download_exp','PdfController@download_exp');

 
 Route:: get('download_morbiT','PdfController@download_morbiT');
	
 Route:: get('download_med','PdfController@download_med');
	
 Route:: get('download_consultas','PdfController@download_consultas');

 Route:: get('download_num','PdfController@download_num');
	
 Route:: get('download_ref','PdfController@download_ref');
	
 Route:: get('download_trauma','PdfController@download_trauma');


	
 














//ruta de recursos para gestionar los usuarios
Route::resource('usuario', 'UsuarioController');
 
