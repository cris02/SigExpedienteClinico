<?php

namespace SigExpCli\Http\Requests;

use SigExpCli\Http\Requests\Request;

class ReferenciaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ref_fecha' => 'required',
        ];
    }
}
