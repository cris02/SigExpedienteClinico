<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use DB;
use PDF;

class PdfController extends Controller
{

//medEspecialidades
	 public function download_medEsp(Request $request)
    {
        if ($request)
          {
            $query=trim($request->get('searchText'));

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_medicamentos')
            ->where('med_especialidad','LIKE','%'.$query.'%')
            ->where('med_fecha','>=','2017-05-05')
            ->where('med_fecha','<=','2017-06-25')
            ->orderBy('med_cant','desc')
            ->limit(10)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_medEsp', compact('data', 'finicio', 'ffinal', 'query', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }




//morbiEspecialidades
    public function download_morbi(Request $request)
    {
        if ($request)
          {
            $query=trim($request->get('searchText'));

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_morbilidadEspecialidades')
            ->where('mde_especialidad','LIKE','%'.$query.'%')
            ->where('mde_fecha','>=','2017-05-05')
            ->where('mde_fecha','<=','2017-06-25')
            ->orderBy('mde_cant','desc')
            ->limit(10)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');
            
            $view =  \View::make('reportePDF.pdf_morbi', compact('data', 'finicio', 'ffinal', 'query', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }  

    }



//cuadroConsulta
    public function download_cuadroConsultas(Request $request)
    {
        if ($request)
          {
            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime('2017-05-05')); 
            $dato1 = explode("-", $fecha1);

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime('2017-06-25'));
            $dato2 = explode("-", $fecha2); 


            $m1 = "Mes";
            $m2 = "Mes";
            
             if (($finicio) And ($ffinal))
             {  
                /*Valida mes de fechas*/
                if (($dato1[1]) < ($dato2[1]))
                 {  
                    if ($dato1[1]=='1'){$m1="Enero";}
                    if ($dato1[1]=='2'){$m1="Febrero";}
                    if ($dato1[1]=='3'){$m1="Marzo";}
                    if ($dato1[1]=='4'){$m1="Abril";}
                    if ($dato1[1]=='5'){$m1="Mayo";}
                    if ($dato1[1]=='6'){$m1="Junio";}
                    if ($dato1[1]=='7'){$m1="Julio";}
                    if ($dato1[1]=='8'){$m1="Agosto";}
                    if ($dato1[1]=='9'){$m1="Septiembre";}
                    if ($dato1[1]=='10'){$m1="Octubre";}
                    if ($dato1[1]=='11'){$m1="Noviembre";}
                    if ($dato1[1]=='12'){$m1="Diciembre";}

                    if ($dato2[1]=='1'){$m2="Enero";}
                    if ($dato2[1]=='2'){$m2="Febrero";}
                    if ($dato2[1]=='3'){$m2="Marzo";}
                    if ($dato2[1]=='4'){$m2="Abril";}
                    if ($dato2[1]=='5'){$m2="Mayo";}
                    if ($dato2[1]=='6'){$m2="Junio";}
                    if ($dato2[1]=='7'){$m2="Julio";}
                    if ($dato2[1]=='8'){$m2="Agosto";}
                    if ($dato2[1]=='9'){$m2="Septiembre";}
                    if ($dato2[1]=='10'){$m2="Octubre";}
                    if ($dato2[1]=='11'){$m2="Noviembre";}
                    if ($dato2[1]=='12'){$m2="Diciembre";}
                 }
            }


            $data=DB::table('tb_cuadroConsultas')
            ->whereMonth('cco_fecha','=',$dato1[1])
            ->whereYear('cco_fecha','=',$dato1[0])
            ->orderBy('cco_cant','desc')
            ->limit(10)->get();

            $data2=DB::table('tb_cuadroConsultas')
            ->whereMonth('cco_fecha','=',$dato2[1])
            ->whereYear('cco_fecha','=',$dato2[0])
            ->orderBy('cco_cant','desc')
            ->limit(10)->get();



            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');
            
            $view =  \View::make('reportePDF.pdf_cuadroConsultas', compact('data', 'data2', 'm1', 'm2', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }  

    }



//cuadroMedicamentos
    public function download_cuadroMed(Request $request)
    {
        if ($request)
          {
            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime('2017-05-05')); 
            $dato1 = explode("-", $fecha1);

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime('2017-06-25'));
            $dato2 = explode("-", $fecha2); 


            $m1 = "Mes";
            $m2 = "Mes";
            
             if (($finicio) And ($ffinal))
             {  
                /*Valida mes de fechas*/
                if (($dato1[1]) < ($dato2[1]))
                 {  
                    if ($dato1[1]=='1'){$m1="Enero";}
                    if ($dato1[1]=='2'){$m1="Febrero";}
                    if ($dato1[1]=='3'){$m1="Marzo";}
                    if ($dato1[1]=='4'){$m1="Abril";}
                    if ($dato1[1]=='5'){$m1="Mayo";}
                    if ($dato1[1]=='6'){$m1="Junio";}
                    if ($dato1[1]=='7'){$m1="Julio";}
                    if ($dato1[1]=='8'){$m1="Agosto";}
                    if ($dato1[1]=='9'){$m1="Septiembre";}
                    if ($dato1[1]=='10'){$m1="Octubre";}
                    if ($dato1[1]=='11'){$m1="Noviembre";}
                    if ($dato1[1]=='12'){$m1="Diciembre";}

                    if ($dato2[1]=='1'){$m2="Enero";}
                    if ($dato2[1]=='2'){$m2="Febrero";}
                    if ($dato2[1]=='3'){$m2="Marzo";}
                    if ($dato2[1]=='4'){$m2="Abril";}
                    if ($dato2[1]=='5'){$m2="Mayo";}
                    if ($dato2[1]=='6'){$m2="Junio";}
                    if ($dato2[1]=='7'){$m2="Julio";}
                    if ($dato2[1]=='8'){$m2="Agosto";}
                    if ($dato2[1]=='9'){$m2="Septiembre";}
                    if ($dato2[1]=='10'){$m2="Octubre";}
                    if ($dato2[1]=='11'){$m2="Noviembre";}
                    if ($dato2[1]=='12'){$m2="Diciembre";}
                 }
            }


            $data=DB::table('tb_cuadroMedicamentos')
            ->whereMonth('cme_fecha','=',$dato1[1])
            ->whereYear('cme_fecha','=',$dato1[0])
            ->orderBy('cme_cant','desc')
            ->limit(10)->get();

            $data2=DB::table('tb_cuadroMedicamentos')
            ->whereMonth('cme_fecha','=',$dato2[1])
            ->whereYear('cme_fecha','=',$dato2[0])
            ->orderBy('cme_cant','desc')
            ->limit(10)->get();



            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');
            
            $view =  \View::make('reportePDF.pdf_cuadroMed', compact('data', 'data2', 'm1', 'm2', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }  

    }



//Expedientes
    public function download_exp(Request $request)
    {
        if ($request)
          {

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_expedientes')
            ->where('exp_fecha','>=','2017-06-01')
            ->where('exp_fecha','<=','2017-06-30')
            ->orderBy('exp_cant','desc')
            ->limit(10)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_exp', compact('data', 'finicio', 'ffinal', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }


    //*  -- Tacticas -- *//


    //morbiEspecialidades
    public function download_morbiT(Request $request)
    {
        if ($request)
          {
            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_morbilidades')
            ->where('mor_fecha','>=','2017-05-20')
            ->where('mor_fecha','<=','2017-06-20')
            ->orderBy('mor_cant','desc')
            ->limit(10)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');
            
            $view =  \View::make('reportePDF.pdf_morbiT', compact('data', 'finicio', 'ffinal', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }  

    }


//medRecetados
     public function download_med(Request $request)
    {
        if ($request)
          {
            $query=trim($request->get('searchText'));
            $con=(int)$query;

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_medicamentoRecetas')
            ->where("mre_cant",">=",$con)
            ->where("mre_fecha",">=",'2017-05-20')
            ->where("mre_fecha","<=",'2017-06-20')
            ->limit(10)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_med', compact('data', 'finicio', 'ffinal', 'con', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }



//consulta
    public function download_consultas(Request $request)
    {
        if ($request)
          {
            $query=trim($request->get('searchText'));
            $finicio=trim($request->get('date'));
            $fecha1=date("Y-m-d", strtotime("$finicio"));

            $fechafin=trim($request->get('date2'));
            $fecha2=date("Y-m-d", strtotime("$fechafin"));

            $data=DB::table('tb_consultas')
            ->where('cta_sexo','LIKE','%'.'Femenino'.'%')
            ->where("cta_fecha",">=",'2017-05-20')
            ->where("cta_fecha","<=",'2017-06-20')
            ->OrderBy('cta_cant','desc')
            ->limit(10)->get(); 


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_consultas', compact('data', 'finicio', 'fechafin', 'query', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');


          }

    }


//Estadistica 
     public function download_num(Request $request)
    {
        if ($request)
          {
            $query=trim($request->get('searchText'));

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            if($query == 'Todos')
                {
                    $data=DB::table('tb_estadisticaconsultas')
                    ->where("est_fecha",">=",'2017-05-20')
                    ->where("est_fecha","<=",'2017-06-20')
                    ->OrderBy('est_cant','desc')
                    ->limit(20)->get();

                }else
                {
                    $data=DB::table('tb_estadisticaconsultas')
                    ->where('est_especialidad','LIKE','%'.$query.'%')
                    ->where('est_fecha','>=','2017-05-20')
                    ->where('est_fecha','<=','2017-06-20')
                    ->OrderBy('est_cant','desc')
                    ->limit(20)->get();
                }



            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_num', compact('data', 'finicio', 'ffinal', 'query', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }


//referencias
     public function download_ref(Request $request)
    {
        if ($request)
          {

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_referencias')
            ->where('ref_fecha','>=','2017-05-20')
            ->where('ref_fecha','<=','2017-06-20')
            ->orderBy('ref_cant','desc')
            ->limit(20)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_ref', compact('data', 'finicio', 'ffinal', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }


//traumatologias
     public function download_trauma(Request $request)
    {
        if ($request)
          {

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            
            $data=DB::table('tb_traumatologias')
            ->where('tra_fecha','>=','2017-05-20')
            ->where('tra_fecha','<=','2017-06-20')
            ->orderBy('tra_cant','desc')
            ->limit(20)->get();


            $nombre = getenv ("REDIRECT_REMOTE_USER");
            $date = date('d-m-Y');

            $view =  \View::make('reportePDF.pdf_trauma', compact('data', 'finicio', 'ffinal', 'date','nombre'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->stream('reportePDF.pdf');
          }
    }



}
