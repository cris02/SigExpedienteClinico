<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\MedEspecialidad;
use SigExpCli\Bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class medEspecialidadController extends Controller
{
    public function __construct(){

    }


    public function index(Request $request){

    	if ($request)

        {
            $query=trim($request->get('searchText'));

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));

            $valFecha = '0';
            
             if (($finicio) And ($ffinal))
             {  
                if (($fecha1) < ($fecha2))
                 {  
                    $valFecha = '2';
                 }
                 else
                 {
                    $valFecha = '1';
                 }

             }
             else
             {
                 $valFecha = '3';
             }

            
            $medEspecialidad=DB::table('tb_medicamentos')
            ->where('med_especialidad','LIKE','%'.$query.'%')
            ->where('med_fecha','>=',$fecha1)
            ->where('med_fecha','<=',$fecha2)
            ->orderBy('med_cant','desc')
            ->paginate(20);



            //bitacora
            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='1';
            $bita->bit_nombreUsuario="Medicamentos Recetados por Especialidad";
            $bita->bit_fecha=$fechaActual;
            $bita->save();


            return view('medEspecialidad.index',["medEspecialidad"=>$medEspecialidad, "valFecha"=>$valFecha, "finicio"=>$finicio, "ffinal"=>$ffinal,"searchText"=>$query]);

        }

    }



}
