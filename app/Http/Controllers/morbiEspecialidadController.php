<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\MorbiEspecialidad;
use SigExpCli\Bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class morbiEspecialidadController extends Controller
{
    public function __construct(){

    }


    public function index(Request $request){

        if ($request)
        {

            $query=trim($request->get('searchText'));

            $finicio=trim($request->get('date'));
            $fecha1= date("Y-m-d", strtotime("$finicio")); 
            $dato1 = explode("-", $fecha1);

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));
            $dato2 = explode("-", $fecha2); 

            $valFecha = '0';
            
             if (($finicio) And ($ffinal))
             {  
                if (($fecha1) < ($fecha2))
                 {  
                    $valFecha = '2';
                 }
                 else
                 {
                    $valFecha = '1';
                 }

             }
             else
             {
                 $valFecha = '3';
             }


            $morbiEspecialidad=DB::table('tb_morbilidadEspecialidades')
            ->where('mde_especialidad','LIKE','%'.$query.'%')
            ->where('mde_fecha','>=',$fecha1)
            ->where('mde_fecha','<=',$fecha2)
            ->orderBy('mde_cant','desc')
            ->paginate(20);



            //bitacora
            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='1';
            $bita->bit_nombreUsuario="Causas de morbilidad por espeicalidad";
            $bita->bit_fecha=$fechaActual;
            $bita->save();


            return view('morbiEspecialidad.index',["morbiEspecialidad"=>$morbiEspecialidad, "valFecha"=>$valFecha, "finicio"=>$finicio, "ffinal"=>$ffinal,"searchText"=>$query]);
        }

    }



}
