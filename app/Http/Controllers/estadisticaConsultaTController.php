<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\estadisticaConsulta;
use SigExpCli\bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class estadisticaConsultaTController extends Controller
{
    //
    public function __construct(){

    }

    public function index(Request $request){

    	if($request){
    		$query=trim($request->get('searchText'));
            $finicio=trim($request->get('date'));
            $fecha1=date("Y-m-d", strtotime("$finicio"));

            $fechafin=trim($request->get('date2'));
            $fecha2=date("Y-m-d", strtotime("$fechafin"));

            if($query == 'Todos')
            	{
            		$estCon=DB::table('tb_estadisticaconsultas')
            		->where("est_fecha",">=",$fecha1)
            		->where("est_fecha","<=",$fecha2)
            		->OrderBy('est_especialidad', 'desc')
		            ->OrderBy('est_cant','desc')
		    		->paginate(20);

            	}else
	            {
		    		$estCon=DB::table('tb_estadisticaconsultas')
		    		->where('est_especialidad','LIKE','%'.$query.'%')
		            ->where("est_fecha",">=",$fecha1)
		            ->where("est_fecha","<=",$fecha2)
		            ->OrderBy('est_cant','desc')
		    		->paginate(20);
	    		}

            $fecha1=date("d-m-Y", strtotime("$fecha1"));
            $fecha2=date("d-m-Y", strtotime("$fecha2"));

            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='2';
            $bita->bit_nombreUsuario="Estadistica de Consulta";
            $bita->bit_fecha=$fechaActual;
            $bita->save();

    		return view('estadisticaConsultaT.index',["estCon"=>$estCon,
    			"searchText"=>$query,
                "date3"=>$fecha1, "date4"=>$fecha2]);
    	}
    }
}
