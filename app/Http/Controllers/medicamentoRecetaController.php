<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;
use SigExpCli\medicamentoRecetado;
use Illuminate\Support\Facades\Redirect;
use SigExpCli\Http\Requests\medicamentoRecetadoRequest;
use DB;
use SigExpCli\bitacora;



class medicamentoRecetaController extends Controller
{
    //Creacion del constructor

    public function __construct(){

    }

    public function index(Request $request){

    	if($request){
            $finicio=trim($request->get('date'));
            $fecha1=date("Y-m-d", strtotime("$finicio"));

            $fechafin=trim($request->get('date2'));
            $fecha2=date("Y-m-d", strtotime("$fechafin"));

    		$query=trim($request->get('searchText'));
            $con=(int)$query;
    		$mediRece=DB::table('tb_medicamentoRecetas')->where("mre_cant",">=",$con)
            ->where("mre_fecha",">=",$fecha1)
            ->where("mre_fecha","<=",$fecha2)
            ->OrderBy('mre_cant', 'desc')
    		->paginate(20);

            $fecha1=date("d-m-Y", strtotime("$fecha1"));
            $fecha2=date("d-m-Y", strtotime("$fecha2"));

            //bitacora
            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='1';
            $bita->bit_nombreUsuario="Medicamento mas recetado";
            $bita->bit_fecha=$fechaActual;
            $bita->save();


            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='1';
            $bita->bit_nombreUsuario="Medicamento mas recetado";
            $bita->bit_fecha=$fechaActual;
            $bita->save();


    		return view('medicamentoRecetado.index',["mediRece"=>$mediRece,"searchText"=>$con,
             "date3"=>$fecha1, "date4"=>$fecha2]);
    	}
    }

    public function show(){

    }

    public function create(){
    	return view("medicamentoRecetado.create");
    }

    public function edit($id){
    	return view("medicamentoRecetado.edit",["mediRece"=>medicamentoRecetado::findOrFail($id)]);
    }


}
