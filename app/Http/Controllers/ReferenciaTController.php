<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;
use SigExpCli\Expediente;
use SigExpCli\Bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class ReferenciaTController extends Controller
{
    public function __construct()
    {
    	# code...
    }

    public function index(Request $request)
    {
    	if ($request) {
    		if($request){
            $finicio=trim($request->get('date'));
            $fecha1=date("Y-m-d", strtotime("$finicio"));
             $dato1 = explode("-", $fecha1); 
             $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));
             $dato2 = explode("-", $fecha2); 

            if (($finicio) And ($ffinal))
             {  
                /*Valida mes de fechas*/
                if (($fecha1) < ($fecha2))
                 {  
                    $valFecha = '2';

                       if ($dato1[1]=='1'){$m1="Enero";}
                    if ($dato1[1]=='2'){$m1="Febrero";}
                    if ($dato1[1]=='3'){$m1="Marzo";}
                    if ($dato1[1]=='4'){$m1="Abril";}
                    if ($dato1[1]=='5'){$m1="Mayo";}
                    if ($dato1[1]=='6'){$m1="Junio";}
                    if ($dato1[1]=='7'){$m1="Julio";}
                    if ($dato1[1]=='8'){$m1="Agosto";}
                    if ($dato1[1]=='9'){$m1="Septiembre";}
                    if ($dato1[1]=='10'){$m1="Octubre";}
                    if ($dato1[1]=='11'){$m1="Noviembre";}
                    if ($dato1[1]=='12'){$m1="Diciembre";}


                 }
                 else
                 {
                    $valFecha = '1';
                 }

             }
             else
             {
                 $valFecha = '3';
             }

            

            

            $referencias=DB::table('tb_referencias')
            ->whereMonth('ref_fecha','=',$dato1[1])
            ->whereYear('ref_fecha','=',$dato1[0])
            ->paginate(20);


            //bitacora
            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='2';
            $bita->bit_nombreUsuario="Referencias Medicas";
            $bita->bit_fecha=$fechaActual;
            $bita->save();


            return view('referenciaT.index',["referencias"=>$referencias,"valFecha"=>$valFecha,"fecha1"=>$finicio,"fecha2"=>$ffinal]);


        }
    	}
    	
    }
}
