<?php

/*
|--------------------------------------------------------------------------
| Application DateController
|--------------------------------------------------------------------------
|
| El código de nuestro controlador únicamente para mostrar
| la información de la variable que quedaría
| En muestro caso es para mostrar la fecha seleccionada en el input 
|
*/


namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
 

class DateController extends Controller
{
    function showDate(Request $request)
    {
       dd($request->date);
    }
}