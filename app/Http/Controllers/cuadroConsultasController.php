<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\CuadroConsultas;
use SigExpCli\Bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class cuadroConsultasController extends Controller
{
    public function __construct(){

    }



    public function index(Request $request){

    	if ($request)
    	{

            $finicio=trim($request->get('date'));
            //corvirtiendo en Date con formato yyyy-mm-dd
            $fecha1= date("Y-m-d", strtotime("$finicio")); 
            //convirtiendo en arrays para extraer valores: (array[0]=yyyy, array[1]=mm, array[3]=dd)
            $dato1 = explode("-", $fecha1);

            $ffinal=trim($request->get('date2'));
            $fecha2= date("Y-m-d", strtotime("$ffinal"));
            $dato2 = explode("-", $fecha2); 


            $m1 = "Mes";
            $m2 = "Mes";
            $valFecha = '0';
            
             if (($finicio) And ($ffinal))
             {  
                /*Valida mes de fechas*/
                if (($dato1[1]) < ($dato2[1]))
                 {  
                    $valFecha = '2';

                    if ($dato1[1]=='1'){$m1="Enero";}
                    if ($dato1[1]=='2'){$m1="Febrero";}
                    if ($dato1[1]=='3'){$m1="Marzo";}
                    if ($dato1[1]=='4'){$m1="Abril";}
                    if ($dato1[1]=='5'){$m1="Mayo";}
                    if ($dato1[1]=='6'){$m1="Junio";}
                    if ($dato1[1]=='7'){$m1="Julio";}
                    if ($dato1[1]=='8'){$m1="Agosto";}
                    if ($dato1[1]=='9'){$m1="Septiembre";}
                    if ($dato1[1]=='10'){$m1="Octubre";}
                    if ($dato1[1]=='11'){$m1="Noviembre";}
                    if ($dato1[1]=='12'){$m1="Diciembre";}

                    if ($dato2[1]=='1'){$m2="Enero";}
                    if ($dato2[1]=='2'){$m2="Febrero";}
                    if ($dato2[1]=='3'){$m2="Marzo";}
                    if ($dato2[1]=='4'){$m2="Abril";}
                    if ($dato2[1]=='5'){$m2="Mayo";}
                    if ($dato2[1]=='6'){$m2="Junio";}
                    if ($dato2[1]=='7'){$m2="Julio";}
                    if ($dato2[1]=='8'){$m2="Agosto";}
                    if ($dato2[1]=='9'){$m2="Septiembre";}
                    if ($dato2[1]=='10'){$m2="Octubre";}
                    if ($dato2[1]=='11'){$m2="Noviembre";}
                    if ($dato2[1]=='12'){$m2="Diciembre";}

                 }
                 else
                 {
                    $valFecha = '1';
                 }

             }
             else
             {
                 $valFecha = '3';
             }

            
            $cuadroConsultas=DB::table('tb_cuadroConsultas')
            ->whereMonth('cco_fecha','=',$dato1[1])
            ->whereYear('cco_fecha','=',$dato1[0])
            ->paginate(20);

            $cuadroConsultas2=DB::table('tb_cuadroConsultas')
            ->whereMonth('cco_fecha','=',$dato2[1])
            ->whereYear('cco_fecha','=',$dato2[0])
            ->paginate(20);

            //bitacora
            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='1';
            $bita->bit_nombreUsuario="Cuadro comparativo de consultas medicas";
            $bita->bit_fecha=$fechaActual;
            $bita->save();
            
                
            return view('cuadroConsultas.index',["cuadroConsultas"=>$cuadroConsultas, "cuadroConsultas2"=>$cuadroConsultas2,"valFecha"=>$valFecha, "mes1"=>$m1, "mes2"=>$m2]);
        } 
    }
}
