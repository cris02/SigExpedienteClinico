<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class bitacoraController extends Controller
{

    //
    public function __construct(){

    }
    
    public function index(Request $request){

    	if($request){
    		$query=trim($request->get('searchText'));
            //$con=(int)$query;
            
    		$bitacora=DB::table('tb_bitacoras')
            ->orwhere('id','like',"%".$query."%")  //"%".$query."%"
            ->OrderBy("bit_fecha", "desc")
            ->paginate(10);
            

    		return view('bitacora.index',["bitacora"=>$bitacora, "searchText"=>$query]);
    	}
    }

    public function show(){

    }

    public function create(){
    	return view("bitacora.create");
    }

    public function edit($id){
    	return view("bitacora.edit",["bitacora"=>Bitacora::findOrFail($id)]);
    }
}
