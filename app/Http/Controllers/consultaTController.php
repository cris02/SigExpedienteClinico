<?php

namespace SigExpCli\Http\Controllers;

use Illuminate\Http\Request;

use SigExpCli\Http\Requests;

use SigExpCli\consulta;
use SigExpCli\bitacora;
use Illuminate\Support\Facades\Redirect;
use DB;

class consultaTController extends Controller
{
    //
    public function __construct(){

    }

    public function index(Request $request){

    	if($request){
    		$query=trim($request->get('searchText'));
            $finicio=trim($request->get('date'));
            $fecha1=date("Y-m-d", strtotime("$finicio"));

            $fechafin=trim($request->get('date2'));
            $fecha2=date("Y-m-d", strtotime("$fechafin"));

    		$consulta=DB::table('tb_consultas')->where('cta_sexo','LIKE','%'.$query.'%')
            ->where("cta_fecha",">=",$fecha1)
            ->where("cta_fecha","<=",$fecha2)
            ->OrderBy('cta_cant','desc')
    		->paginate(20);

            $fecha1=date("d-m-Y", strtotime("$fecha1"));
            $fecha2=date("d-m-Y", strtotime("$fecha2"));

            $fechaActual=date('Y-m-d');
            
            $biId=DB::table('tb_bitacoras')->count('bit_id');
            
            $bita= new bitacora;
            $bita->bit_id=(int)($biId+1);
            $bita->id='2';
            $bita->bit_nombreUsuario="Division por conosulta";
            $bita->bit_fecha=$fechaActual;
            $bita->save();

    		return view('consultaT.index',["consulta"=>$consulta,"searchText"=>$query,
                "date3"=>$fecha1, "date4"=>$fecha2]);
    	}
    }

    public function show(){

    }

    public function create(){
    	return view("consulta.create");
    }

    public function edit($id){
    	return view("consulta.edit",["consulta"=>Consulta::findOrFail($id)]);
    }
}
