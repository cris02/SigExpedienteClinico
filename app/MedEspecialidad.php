<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class MedEspecialidad extends Model
{
    protected $table= 'tb_medicamentos';
    protected $primaryKey= 'med_id';
    public $timestamps=false;


    protected $fillable = [
    	'med_nombre',
    	'med_especialidad',
    	'med_fecha'
    ];

    protected $guarded = [

    ];

}
