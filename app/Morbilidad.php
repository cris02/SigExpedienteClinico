<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class Morbilidad extends Model
{
      protected $table='tb_morbilidades';
    protected $primaryKey='mor_id';
    public $timestamps=false;

    protected $fillable=[
    		'mor_cant',
    		'mor_causas',
    		'mor_fecha',
    		];

    protected $guarded=[
    ];
}
