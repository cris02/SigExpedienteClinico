<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class consulta extends Model
{
    //
    protected $table= 'tb_consultas';
    protected $primaryKey='cta_id';
    public $timestamps=false;

    protected $fillable = [
    	'cta_sexo',
    	'cta_edad',
    	'cta_fecha',
        'cta_cant'
    ];

    protected $guarded=[

    ];
}
