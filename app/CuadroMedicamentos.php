<?php

namespace SigExpCli;

use Illuminate\Database\Eloquent\Model;

class CuadroMedicamentos extends Model
{
    protected $table= 'tb_cuadroMedicamentos';
    protected $primaryKey= 'cme_id';
    public $timestamps=false;


    protected $fillable = [
    	'cme_nombre',
    	'cme_fecha',
    	'updated_at'
    ];

    protected $guarded = [

    ];
}
