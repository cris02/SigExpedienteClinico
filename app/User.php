<?php

namespace SigExpCli;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

     protected $table = 'users';
     protected $primaryKey = 'id';
     //public $timestamps=false;

    protected $fillable = [
        'name', 'email', 'password', 'type', 'condicion',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
