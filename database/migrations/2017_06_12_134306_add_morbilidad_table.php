<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorbilidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_morbilidades', function (Blueprint $table) {
             $table->increments('mor_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('mor_causas', 50);
            $table->date('mor_fecha');
            $table->integer('mor_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('mor_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_morbilidades');
    }
}
