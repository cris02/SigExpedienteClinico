<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTraumatologiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_traumatologias', function (Blueprint $table) {
            $table->increments('tra_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('tra_causas', 60);
            $table->date('tra_fecha');
            $table->integer('tra_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('tra_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_traumatologias');
    }
}
