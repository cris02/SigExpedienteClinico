<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_referencias', function (Blueprint $table) {
            $table->increments('ref_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('ref_causa', 60);
            $table->string('ref_especialidad', 40);
            $table->date('ref_fecha');
            $table->integer('ref_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('ref_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_referencias');
    }
}
