<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCuadromedicamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_cuadroMedicamentos', function (Blueprint $table) {
            $table->increments('cme_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('cme_nombre', 60);
            $table->date('cme_fecha');
            $table->integer('cme_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('cme_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_cuadroMedicamentos');
    }
}
