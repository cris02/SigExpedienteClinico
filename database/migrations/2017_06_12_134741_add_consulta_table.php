<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsultaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_consultas', function (Blueprint $table) {
             $table->increments('cta_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('cta_sexo', 10);
            $table->integer('cta_edad');
            $table->date('cta_fecha');
            $table->integer('cta_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('cta_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_consultas');
    }
}
