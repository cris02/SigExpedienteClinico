<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCuadroconsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_cuadroConsultas', function (Blueprint $table) {
            $table->increments('cco_id');
            $table->integer('id')->unsigned();
            $table->string('cco_general', 40);
            $table->string('cco_especialidad', 40);
            $table->date('cco_fecha');
            $table->integer('cco_cant');
            $table->timestamps();

            //definicion de llave primaria
           // $table->primary('cco_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
         });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_cuadroConsultas');
    }
}
