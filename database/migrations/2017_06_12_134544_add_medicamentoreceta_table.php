<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicamentorecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_medicamentoRecetas', function (Blueprint $table) {
           $table->increments('mre_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('mre_nombre', 40);
            $table->date('mre_fecha');
            $table->integer('mre_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('mre_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_medicamentoRecetas');
    }
}
