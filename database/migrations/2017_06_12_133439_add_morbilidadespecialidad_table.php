<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorbilidadespecialidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_morbilidadEspecialidades', function (Blueprint $table) {
            $table->increments('mde_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('mde_causas', 60);
            $table->string('mde_especialidad', 40);
            $table->date('mde_fecha');
            $table->integer('mde_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('mde_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_morbilidadEspecialidades');
    }
}
