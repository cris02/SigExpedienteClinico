<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpedienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_expedientes', function (Blueprint $table) {
            $table->increments('exp_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('exp_sexo', 20);
            $table->integer('edad');
            $table->date('exp_fecha');
            $table->integer('exp_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('exp_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_expedientes');
    }
}
