<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_medicamentos', function (Blueprint $table) {
            $table->increments('med_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('med_especialidad', 40);
            $table->string('med_nombre', 40);
            $table->date('med_fecha');
            $table->integer('med_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('med_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_medicamentos');
    }
}
