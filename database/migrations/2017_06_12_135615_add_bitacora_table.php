<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_bitacoras', function (Blueprint $table) {
            $table->increments('bit_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('bit_nombreUsuario', 60);
            $table->date('bit_fecha');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('bit_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_bitacoras');
    }
}
