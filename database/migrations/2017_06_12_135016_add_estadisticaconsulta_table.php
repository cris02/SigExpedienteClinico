<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadisticaconsultaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_estadisticaConsultas', function (Blueprint $table) {
           $table->increments('est_id');
            $table->integer('id')->unsigned(); // llave foranea
            $table->string('est_nombreMedico', 40);
            $table->string('est_especialidad', 40);
            $table->date('est_fecha');
            $table->integer('est_cant');
            $table->timestamps();

            //definicion de llave primaria
            //$table->primary('est_id');

            //definicion de referencia a la tabla usuario
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_estadisticaConsultas');
    }
}
