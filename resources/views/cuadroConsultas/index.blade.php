@extends ('layouts.vistaForm')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Cuadro Comparativo de Consultas Medicas</h3></center>
			<br>

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Mostrara la estadística de la cantidad de consultas que se han realizado en las especialidades, en un periodo específico.</h5>
			
					<!-- Mostrar mensajes hasta dar click -->
					@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
						@if ( $valFecha == '3')
							<div class="alert alert-danger" role="alert">Advertencia!: "Debe ingresar las dos fechas para obtener el resultado esperado"</div>
						@endif	

						@if ( $valFecha == '1')

							<div class="alert alert-danger" role="alert">El mes 1 tiene que ser menor que el Mes 2</div>

						@endif	

						@if ( $valFecha == '2')
							<div class="alert alert-success" role="alert">Excelente!: "Información Procesada con exito!"</div>
						@endif

					@endif								

					
					<h5><b>Parametros:</b></h5>
					@include('cuadroConsultas.datepicker')
				</div>
			</div>


			<!-- Mostrar form:solucion hasta dar click -->
			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

				@if ( $valFecha == '2')
					
			
					<div class="form-group">
						<center>

							<div class="panel panel-primary">
  								<div class="panel-heading">
    								<h3 class="panel-title">Cuadro comparativo</h3>
  								</div>

							<form class="form-horizontal" style="float: left">
								<br>
  				  				<fieldset>
  				  				<h4 style="color:orange">Mes 1: {{$mes1}}</h4>
    							<legend></legend>
    								<div class="form-group">

  										<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">

													<table class="table table-striped table-bordered table-condensed table-hover">
														<div class="col-md-12">
															<thead>
																<th>N°</th>
																<th>Especialidad</th>
																<th>Número de Consultas</th>
															</thead><?php $sum = 0; ?>

															@foreach ($cuadroConsultas as $key=>$cuadro)
																<tr><tr><?php $sum += $cuadro->cco_cant; ?>
																	<td>{{ ++$key }}</td>
																	<td>{{$cuadro->cco_especialidad}}</td>
																	<td>{{$cuadro->cco_cant}}</td>
																</tr>
															@endforeach
														</div>
														
													</table>
													<h3 class="panel-title"><b>Total Consultas: </b> {{$sum}}
												</div>
												{{$cuadroConsultas->render()}}
												
											</div>	
										</div>
										</div>

									</div>
  								</fieldset>
							</form>

				
							<form class="form-horizontal" style="float: left">
							<br><div class="col-md-offset-1">

  				  				<fieldset>
  				  				<h4 style="color:orange">Mes 2: {{$mes2}}</h4>
    							<legend></legend>
    								<div class="form-group">

  										<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="table-responsive">

													<table class="table table-striped table-bordered table-condensed table-hover">
														<thead>
															<th>N°</th>
															<th>Especialidad</th>
															<th>Número de Consulta</th>
														</thead><?php $sum = 0; ?>


														@foreach ($cuadroConsultas2 as $key2=>$cuadro2)
														<tr><?php $sum += $cuadro2->cco_cant; ?>
															<td>{{ ++$key2 }}</td>
															<td>{{$cuadro2->cco_especialidad}}</td>
															<td>{{$cuadro2->cco_cant}}</td>
														</tr>
														@endforeach
														
													</table>
													<h3 class="panel-title"><b>Total Consultas: </b> {{$sum}}
												</div>
												{{$cuadroConsultas2->render()}}

											</div>	
										</div>
										</div>

									</div>
  								</fieldset>

  							</div>
							</form>

							</div>

						</center>
					</div>

				@endif
				
			@endif
		</div>

	</div>

@endsection