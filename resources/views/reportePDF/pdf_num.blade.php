@extends ('reportePDF.headPDF')

@section ('contenido')


<center>
	<br>
	<h4>Reporte</h4>
	<h4><b>Estadisticas de Consultas Medicas</b></h4>
</center>

<!--
<h5><b>Especialidad: </b>{{$query}}</h5> 
<h5><b>Entre la fechas </b>{{$finicio}}<b> y </b> {{$ffinal}}</h5>
-->

<div align="center">
	<table class="table table-striped table-hover ">
  		<thead>
    		<tr>
    		  	<th style="text-align:center;" width="80">Especialidad</th>
    		  	<th style="text-align:left;" width="200">Doctor</th>
   		   	  	<th style="text-align:center;" width="80">Cantidad de consultas atendidas</th>
   		 	</tr>
  		</thead>

  		<tbody>
		@foreach ($data as $value)
			<tr>
      			<td style="text-align:center;" width="80">{{$value->est_especialidad}}</td>
 				<td style="text-align:left;" width="200">{{$value->est_nombreMedico}}</td>
				<td style="text-align:center;" width="80">{{$value->est_cant}}</td>
    		</tr>
   		@endforeach
  		</tbody>
	</table> 
</div>

@endsection