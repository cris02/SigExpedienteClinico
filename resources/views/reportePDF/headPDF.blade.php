<!DOCTYPE html>
<html lang="es">

<head>
<link rel="stylesheet" href="public/css/bootstrap.min.css">
<link rel="stylesheet" href="public/css/bootstrap.css">
</head>


<body>
	<div class="row">
		<div class="col-md-10 col-md-offset-0">
			<img src="img/Bienestar_logo2.png">
		</div>
	</div>


	<div class="row">
		<!-- <div style="text-align:left;" class="date">Usuario: {{ $nombre }}</div> -->
		<div style="text-align:right" class="date">Fecha de impresión: {{ $date }}</div>
	</div>


	<center>
		<h3 class="titulo">Sistema Gerencial de Expedientes Clinicos</h3>
	</center>


	<!--Contenido-->
    @yield('contenido')
    <!--Fin Contenido-->


<!-- Esilos tabla -->
<style type="text/css">
	table {
    border-collapse: collapse;
    margin-left: 80px;
	}

	table, th, td {
    border: 1px solid black;
	}
</style>


</body>
</html>
