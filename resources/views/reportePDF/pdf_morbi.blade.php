@extends ('reportePDF.headPDF')

@section ('contenido')

<center>	
	<br>
	<h4>Reporte</h4>
	<h4><b>Causas de Morbilidad por Especialidad</b></h4>
</center>

<div align="center">
	<table class="table table-striped table-hover ">
  		<thead>
    		<tr>
    		  	<th style="text-align:center;" width="80">N°</th>
    		  	<th style="text-align:left;" width="200">Causas de Morbilidad</th>
   		   	  <th style="text-align:center;" width="80">Cantidad</th>
   		 	</tr>
  		</thead>

  		<tbody>
		@foreach ($data as $key=>$value)
			<tr>
      	<td style="text-align:center;" width="80">{{ ++$key }}</td>
 				<td style="text-align:left;" width="200">{{$value->mde_nombre}}</td>
				<td style="text-align:center;" width="80">{{$value->mde_cant}}</td>
    		</tr>
   		@endforeach
  		</tbody>
	</table> 
</div>

@endsection
