@extends ('reportePDF.headPDF')

@section ('contenido')

<center>	
	<br>
	<h4>Reporte</h4>
	<h4><b>Medicamentos recetados por Especialista</b></h4>
</center>

<!--
<div style="text-align:left;" class="text-align"><b>Especialista: </b>{{ $query }}</div>
<div style="text-align:left;" class="date"><b>Periodo de Evaluación:</b> {{ $finicio }} al {{ $ffinal }}</div>
-->

<div align="center">
	<table class="table table-striped table-hover ">
  		<thead>
    		<tr>
    		  	<th style="text-align:center;" width="80">N°</th>
    		  	<th style="text-align:left;" width="200">Medicamento</th>
   		   	  	<th style="text-align:center;" width="80">Cantidad</th>
   		 	</tr>
  		</thead>

  		<tbody>
		@foreach ($data as $key=>$value)
			<tr>
      			<td style="text-align:center;" width="80">{{ ++$key }}</td>
 				<td style="text-align:left;" width="200">{{$value->med_nombre}}</td>
				<td style="text-align:center;" width="80">{{$value->med_cant}}</td>
    		</tr>
   		@endforeach
  		</tbody>
	</table> 
</div>

@endsection
