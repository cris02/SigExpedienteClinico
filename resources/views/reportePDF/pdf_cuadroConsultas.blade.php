@extends ('reportePDF.headPDF')

@section ('contenido')

<center>	
	<br>
	<h4>Reporte</h4>
	<h4><b>Cuadro Comparativo de Consultas atendidas</b></h4>
</center>

<!--
<div style="text-align:left;" class="date"><b>Periodo de Comparacion:</b> {{ $m1 }} y {{ $m2 }}</div>
-->

<span class="input-group-btn">
<div align="center">
	<table class="table table-striped table-hover ">
  		<thead>
    		<tr>
    		  	<th style="text-align:center;" width="30">N°</th>
    		  	<th style="text-align:left;" width="100">Especialidad</th>
   		   	  	<th style="text-align:center;" width="50">N° de Consultas</th>
   		 	</tr>
  		</thead>

  		<tbody>
		@foreach ($data as $key=>$value)
			<tr>
      	<td style="text-align:center;" width="30">{{ ++$key }}</td>
 				<td style="text-align:left;" width="100">{{$value->cco_especialidad}}</td>
				<td style="text-align:center;" width="50">{{$value->cco_cant}}</td>
    		</tr>
   		@endforeach
  		</tbody>
	</table> 


  <table class="table table-striped table-hover ">
      <thead>
        <tr>
            <th style="text-align:center;" width="30">N°</th>
            <th style="text-align:left;" width="100">Especialidad</th>
            <th style="text-align:center;" width="50">N° de Consultas</th>
        </tr>
      </thead>

      <tbody>
    @foreach ($data2 as $key2=>$value2)
      <tr>
        <td style="text-align:center;" width="30">{{ ++$key2 }}</td>
        <td style="text-align:left;" width="100">{{$value2->cco_especialidad}}</td>
        <td style="text-align:center;" width="50">{{$value2->cco_cant}}</td>
        </tr>
      @endforeach
      </tbody>
  </table>
</div>
</span>

@endsection
