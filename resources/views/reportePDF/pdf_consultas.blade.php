@extends ('reportePDF.headPDF')

@section ('contenido')


<center>
	<br>
	<h4>Reporte</h4>
	<h4><b>Division por Consultas Medicas</b></h4>
</center>


<h5><b>Consulta: </b>{{$query}}</h5> 
<h5><b>Entre la fechas </b>{{$finicio}}<b> y </b> {{$fechafin}}</h5>


<div align="center">
	<table class="table table-striped table-hover ">
  		<thead>
    		<tr>
    		  	<th style="text-align:center;" width="80">Sexo</th>
    		  	<th style="text-align:left;" width="200">Edad</th>
   		   	  	<th style="text-align:center;" width="80">Cantidad</th>
   		 	</tr>
  		</thead>

  		<tbody>
		@foreach ($data as $value)
			<tr>
      			<td style="text-align:center;" width="80">{{$value->cta_sexo }}</td>
 				<td style="text-align:left;" width="200">{{$value->cta_edad}}</td>
				<td style="text-align:center;" width="80">{{$value->cta_cant}}</td>
    		</tr>
   		@endforeach
  		</tbody>
	</table> 
</div>

@endsection
