@extends ('layouts.vistaForm')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Medicamentos mas Recetados</h3></center>
			


			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Se mostrara una lista con las veces que se ha recetado un medicamento como minimo </h5>
			
					<b>Parametros:</b>
					
					@include('medicamentoRecetado.datepicker')



				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

			<div class="panel panel-primary">
  				<div class="panel-heading">


    				<h3 class="panel-title"><b>Medicamentos recetados como minimo </b> {{$searchText}} 
    				<b> veces. Entre las fechas </b>{{$date3}} 

    				<b> y </b> {{$date4}}
    				</h3>
  				</div>


  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										
										<th>Nombre del medicamento</th>
										<th>Cantidad</th>
										
									</thead><?php $sum = 0; ?>

									@foreach ($mediRece as $medRe)
										<tr><?php $sum += $medRe->mre_cant; ?>
										
											<td>{{$medRe->mre_nombre}}</td>
											<td>{{$medRe->mre_cant}}</td>
										</tr>
										
									@endforeach
								</table>
								<h3 class="panel-title"><b>Total de pacientes: </b> {{$sum}}

							</div>
							{{$mediRece->render()}}
						</div>	
					</div>
				</div>
			</div>

			@endif

		</div>
	</div>

@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
	<div class="form-group">
      	<div class="col-md-offset-10">
			<a class="btn btn-danger" align="right" href="#">Imprimir</a>
		</div>
	</div>
	@endif

@endsection