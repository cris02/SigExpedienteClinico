@extends ('layouts.vistaForm2')


@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Causas mas frecuentes de Traumatologías</h3></center>
			

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parámetros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Se muestran las causas más frecuentes que generan traumatologías a los pacientes.</h5>
			<!-- Mostrar mensajes hasta dar click -->
					@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
						@if ( $valFecha == '3')
							<div class="alert alert-danger" role="alert">Debe ingresar las dos fechas</div>
						@endif	

						@if ( $valFecha == '1')
							<div class="alert alert-danger" role="alert">La Fecha de Inicio debe ser menor que la Fecha Fin</div>
						@endif		
					@endif	

					<b>Parametros:</b>
					@include('traumatologiaT.datepicker')
				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
@if ( $valFecha == '2')
			<div class="panel panel-primary">

  				 


  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										<th>Causa de Traumatología</th>
										<th>Cantidad</th>
										
										
									</thead>

									@foreach ($traumatologias as $trauma)
										<tr>
											<td>{{$trauma->tra_causas}}</td>
											<td>{{$trauma->tra_cant}}</td>
											
											
										</tr>
									@endforeach
								</table>

							</div>
							{{$traumatologias->render()}}
						</div>	
					</div>
				</div>
			</div>

			@endif
@endif
		</div>
	</div>


@endsection