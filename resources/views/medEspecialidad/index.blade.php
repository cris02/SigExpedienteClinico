@extends ('layouts.vistaForm')

@section ('contenido')


	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Medicamentos recetados por Especialidad</h3></center>
			


			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Mostrara la estadística de la cantidad de medicamentos recetados en orden descendente (mayor a menor) en las consultas que se han realizado por cada especialista en un periodo de tiempo.</h5>
			

					<!-- Mostrar mensajes hasta dar click -->
					@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
						@if ( $valFecha == '3')
							<div class="alert alert-danger" role="alert">Advertencia!: "Debe ingresar las dos fechas para obtener el resultado esperado"</div>
						@endif	

						@if ( $valFecha == '1')
							<div class="alert alert-danger" role="alert">Advertencia!: "El mes de la fecha inicial del periodo tiene que ser menor que el mes de la fecha final del periodo2</div>
						@endif	

						@if ( $valFecha == '2')
							<div class="alert alert-success" role="alert">Excelente!: "Información Procesada con exito!"</div>
						@endif

					@endif


					
					<b>Parametros:</b>

					@include('medEspecialidad.search')

				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

			  @if ( $valFecha == '2')

			  <h5><b>Periodo de Evaluación:</b> {{$finicio}} al {{$ffinal}}</h5>
			  
			   <div class="panel panel-primary">
  				 <div class="panel-heading">
    				<h3 class="panel-title"><b>Especialista: </b>{{$searchText}}</h3>
  				 </div>

  				 <div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										<th>N°</th>
										<th>Medicamento</th>
										<th>Cantidad</th>
										
									</thead><?php $sum = 0; ?>

									@foreach ($medEspecialidad as $key=>$med)
										<tr><?php $sum += $med->med_cant; ?>
											<td>{{ ++$key }}</td>
											<td>{{$med->med_nombre}}</td>
											<td>{{$med->med_cant}}</td>
										</tr>
									@endforeach
								</table>
								<h3 class="panel-title"><b>Total de medicamentos: </b> {{$sum}}
							</div>
							{{$medEspecialidad->render()}}
						</div>	
					</div>
				  </div>
			   </div>
			 @endif
			@endif

		</div>
	</div>

@endsection