{!! Form::open(array('url'=>'estadisticaConsultaT','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<!DOCTYPE html>
<html>
<head>
    <title>Datepicker</title>
 
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-standalone.css')}}">
    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

</head>

<body>
    <div class="row">
        <div class=" col-lg-4">
                <br><h5>Seleccione:</h5><br>
                <select name="searchText" id="input-consulta" class="form-control" value="{{$searchText}}">
                <option>-- Especialidad --</option>
                <option>General</option>
                <option>Pediatría</option>
                <option>Alergología</option>
                <option>Cardiología</option>
                <option>Gastroenterología</option>
                <option>Oftalmología</option>
                <option>Urología</option>
                <option>Dermatología</option>
                <option>Ginecología</option>
                <option>Otorrinolaringología</option>
                <option>Todos</option>
                </select>
            </div>

            <div class="col-lg-4">
                <h5>Fecha Inicio</h5>  
                <input type="text" class="form-control datepicker" name="date">
            </div>
            <div class="col-lg-4">
                <br><h5>Fecha Fin</h5>
                <input type="text" class="form-control datepicker" name="date2">    
            </div>
        
        <div class="row">
            <div class="col-lg-12">
                <center>
            <span class="input-group-datepicer">
                <button type="submit" name="mostrar" class="btn btn-primary" value="Ok">Calcular</button>
                <a id="bt_pdf" class="btn btn-danger" type="submit" align="right" href="{{url('download_div')}}" target="blank">Imprimir</a>
            </span>
            </center>
            </div>
            </div>

           
    </div>

<script>
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
</script>
</body>
</html>

{{Form::close()}}