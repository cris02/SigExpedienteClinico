@extends ('layouts.vistaForm2')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Estadistica de Consulta Medica</h3></center>


			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Se mostrara una lista de las diferentes especialidades, ademas de dos fechas
					que son el periodo de filtrado. A traves de estos parametro se mostrara una lista 
					de doctores por especialidad y cant de consulta que han atendido.</h5>
			
					<br>
					<b>Parametros:</b>
					
					@include('estadisticaConsultaT.datepicker')



				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title"><b>Especialidad: </b>{{$searchText}}
    				<b> Entre las fechas </b>{{$date3}} 
    				<b> y </b> {{$date4}}
    				</h3>
  				</div>
  					

  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
							

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										
										<th>Especialidad</th>
										<th>Doctor</th>
										<th>cantidad</th>
									</thead>

									@foreach ($estCon as $escon)
										<tr>
											
											<td>{{ $escon->est_especialidad}}</td>
											<td>{{ $escon->est_nombreMedico}}</td>
											<td>{{ $escon->est_cant}}</td>
										</tr>
									@endforeach
								</table>

							</div>
							{{$estCon->render()}}
						</div>	
					</div>
				</div>
			</div>

			@endif

		</div>
	</div>

@endsection