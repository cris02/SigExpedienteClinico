{!! Form::open(array('url'=>'morbilidadT','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<!DOCTYPE html>
<html>
<head>
    <title>Datepicker</title>
 
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-standalone.css')}}">
    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

</head>


<body>
	<div class="row">
		<div class="col-md-6 ">
				<div class="form-group ">
  					<label class="control-label" for="inputWarning">Fecha Inicio</label>
  					<input type="text" class="form-control datepicker" name="date">
				</div>
			
		</div>
		<div class="col-md-6 ">
				<div class="form-group ">
				
  					<label class="control-label" for="inputWarning">Fecha Fin</label>
  					<input type="text" class="form-control datepicker" name="date2">
				</div>
			</div>

			<div class="col-md-12 ">
		<div class="form-group ">
			<center>
			<span class="input-group-btn">
				<button type="submit" name="mostrar" class="btn btn-primary" value="Ok">Vista Previa</button>

				<a id="bt_pdf" class="btn btn-danger" type="submit" align="right" href="{{url('download_morbiT')}}" target="blank">Imprimir</a>
			</span>
		</center>
	</div>
	</div>

<script>
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
</script>
</body>
</html>

{{Form::close()}}