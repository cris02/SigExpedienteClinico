@extends ('layouts.vistaForm2')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Division de Consulta Medica</h3></center>
			


			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Mostrara como se ha divido la consulta dependiendo del sexo del paciente, la edad y un periodo determinado.</h5>
			
					<h5><b>Parametros:</b></h5>
					
					@include('consultaT.datepicker')



				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title"><b>Consulta: </b>{{$searchText}}
    				<b> Entre la fechas </b>{{$date3}} 
    				<b> y </b> {{$date4}}
    				</h3>
  				</div>
  					

  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
							

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										
										<th>sexo</th>
										<th>edad del paciente</th>
										<th>cantidad</th>
									</thead>

									@foreach ($consulta as $con)
										<tr>
											
											<td>{{ $con->cta_sexo}}</td>
											<td>{{ $con->cta_edad}}</td>
											<td>{{ $con->cta_cant}}</td>
										</tr>
									@endforeach
								</table>

							</div>
							{{$consulta->render()}}
						</div>	
					</div>
				</div>
			</div>

			@endif

		</div>
		@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
	<div class="form-group">
      	<div class="col-md-offset-10">
			<a class="btn btn-danger" align="right" href="#">Imprimir</a>
		</div>
	</div>
	@endif
	</div>



@endsection