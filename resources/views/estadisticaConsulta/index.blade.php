@extends ('layouts.vistaForm')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Estadistica de Consulta Medica</h3></center>
			


			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Se mostrara una lista de las diferentes especialidades, ademas de dos fechas
					que son el periodo de filtrado. A traves de estos parametro se mostrara una lista 
					de doctores por especialidad y cant de consulta que han atendido.</h5>
			
					
					<b>Parametros:</b>

					
					@include('estadisticaConsulta.datepicker')



				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title"><b>Especialidad: </b>{{$searchText}}
    				<b> Entre la fechas </b>{{$date3}} 
    				<b> y </b> {{$date4}}
    				</h3>
  				</div>
  					

  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
							

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										
										<th>Especialidad</th>
										<th>Doctor</th>
										<th>Cantidad de consultas atendidas</th>
									</thead><?php $sum = 0; ?>

									@foreach ($estCon as $escon)
										<tr><?php $sum += $escon->est_cant; ?>							
											<td>{{ $escon->est_especialidad}}</td>
											<td>{{ $escon->est_nombreMedico}}</td>
											<td>{{ $escon->est_cant}}</td>
										</tr>
									@endforeach
								</table>
<h3 class="panel-title"><b>Total de consultas atendidas: </b> {{$sum}}
							</div>
							{{$estCon->render()}}
						</div>	
					</div>
				</div>
			</div>

			@endif

		</div>
	</div>

<a id="bt_pdf" class="btn btn-danger" type="submit" align="right" href="{{url('download_num')}}" target="blank">Imprimir</a>

@endsection