<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$usu->id}}">
	{{Form::Open(array('action'=>array('UsuarioController@destroy',$usu->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
                     <span aria-hidden="true">X</span>
                </button>
                <h3 class="modal-title">Eliminar Usuario</h3>
			</div>
			<div class="modal-body">
				<h4 class="text-center">Confirme si desea Eliminar: </h4>
				<p><strong>Nombre:</strong> {{$usu->name}}</p>
				<p><strong>Correo:</strong> {{$usu->email}}</p>
				<p><strong>Tipo de Usuario:</strong> {{$usu->type}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-danger">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
