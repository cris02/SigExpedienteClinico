@extends ('layouts.admin3')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nuevo Usuario</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::open(array('url'=>'usuario','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name" class="col-md-10 control-label">Nombre</label>

				<div class="col-md-10">
					<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

					@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-grup col-md-10">
				<br>
			</div>

			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email" class="col-md-10 control-label">Correo</label>

				<div class="col-md-10">
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-grup col-md-10">
				<br>
			</div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="col-md-10 control-label">Contraseña</label>

				<div class="col-md-10">
					<input id="password" type="password" class="form-control" name="password">

					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-grup col-md-10">
				<br>
			</div>

			<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
				<label for="password-confirm" class="col-md-10 control-label">Confirmar Contraseña</label>

				<div class="col-md-10">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation">

					@if ($errors->has('password_confirmation'))
						<span class="help-block">
							<strong>{{ $errors->first('password_confirmation') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-grup col-md-10">
				<br>
			</div>

		<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
			<label for="tipo" class="col-md-8 control-label">Tipo de Usuario</label>

			<div class="col-md-10" >
				{!! Form::select('type',[''=>'Seleccione un tipo de usuario','administrador'=>'Administrador del Sistema','gerencial'=>'Usuario Gerencial','tactico'=>'Usuario Tactico'],null,['class'=>'form-control'])!!}
			</div>
				@if ($errors->has('type'))
				<span class="help-block">
					<strong>{{ $errors->first('type') }}</strong>
				</span>
				@endif
		</div>

		<div class="form-grup col-md-10">
				<br>
			</div>

            <div class="form-group col-md-8">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

			{!!Form::close()!!}

		</div>
	</div>
@endsection
