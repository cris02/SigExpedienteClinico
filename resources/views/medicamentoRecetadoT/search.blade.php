{!! Form::open(array('url'=>'mediRece','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}


<!--
<div class="form-group">
	<input type="text" class="form-control" name="searchText" placeholder="Buscar..." value="{{$searchText}}">
	<span class="input-group-btn">
		<button type="submit" class="btn btn-primary">Buscar</button>
	</span>
</div>

-->
<div class="row">
  <div class="col-lg-5">

	<div class="form-group">
		<div class="input-group">
			<select name="searchText" id="input-consulta" class="form-control" value="{{$searchText}}">
				<option>-- Seleccione el numero minimo --</option>
				<option > 10 </option>
				<option > 20 </option>
				<option  > 30 </option>
				<option > 40 </option>
				<option > 50 </option>
			</select>
			
			

			<span class="input-group-datepicer">
				<button type="submit" name="mostrar" class="btn btn-primary" value="Ok">Calcular</button>
				<a id="bt_pdf" class="btn btn-danger" type="submit" align="right" href="{{url('download_med')}}" target="blank">Imprimir</a>
			</span>

		</div>
	</div>

  </div>
</div>

<script>
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
</script>

{{Form::close()}}