@extends ('layouts.vistaForm')

@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Cuadro Comparativo de Medicamentos Recetados</h3></center>
			

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parametros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Mostrara la estadística de todos los medicamentos recetados en un mes comparados con las de otro mes. .</h5>
			
					<!-- Mostrar mensajes hasta dar click -->
					@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
						@if ( $valFecha == '3')
							<div class="alert alert-danger" role="alert">Advertencia!: "Debe ingresar las dos fechas para obtener el resultado esperado"</div>
						@endif	

						@if ( $valFecha == '1')
							<div class="alert alert-danger" role="alert">Advertencia!: "El mes de la fecha inicial del periodo tiene que ser menor que el mes de la fecha final del periodo2</div>
						@endif	

						@if ( $valFecha == '2')
							<div class="alert alert-success" role="alert">Excelente!: "Información Procesada con exito!"</div>
						@endif

					@endif								

					
					<b>Parametros:</b>
					@include('cuadroMedicamentos.datepicker')
				</div>
			</div>


			<!-- Mostrar form:solucion hasta dar click -->
			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')

				@if ( $valFecha == '2')
					
			
					<div class="form-group">
						<center>

							<div class="panel panel-primary">
  								<div class="panel-heading">
    								<h3 class="panel-title">Cuadro comparativo</h3>
  								</div>

							<form class="form-horizontal" style="float: left">
								<br>
  				  				<fieldset>
  				  				<h4 style="color:orange">Mes 1: {{$mes1}}</h4>
    							<legend></legend>
    								<div class="form-group">

  										<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">

													<table class="table table-striped table-bordered table-condensed table-hover">
														<div class="col-md-12">
															<thead>
																<th>N°</th>
																<th>Medicamento</th>
																<th>Cantidad</th>
															</thead><?php $sum = 0; ?>

															@foreach ($cuadroMedicamentos as $key=>$cuadro)
																<tr><?php $sum += $cuadro->cme_cant; ?>
																	<td>{{ ++$key }}</td>
																	<td>{{$cuadro->cme_nombre}}</td>
																	<td>{{$cuadro->cme_cant}}</td>
																</tr>
															@endforeach
														</div>
													</table>
													<h3 class="panel-title"><b>Total Medicamentos: </b> {{$sum}}
												</div>
												{{$cuadroMedicamentos->render()}}
												
											</div>	
										</div>
										</div>

									</div>
  								</fieldset>
							</form>

				
							<form class="form-horizontal" style="float: left">
							<br><div class="col-md-offset-1">

  				  				<fieldset>
  				  				<h4 style="color:orange">Mes 2: {{$mes2}}</h4>
    							<legend></legend>
    								<div class="form-group">

  										<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="table-responsive">

													<table class="table table-striped table-bordered table-condensed table-hover">
														<thead>
															<th>N°</th>
															<th>Medicamento</th>
															<th>Cantidad</th>
														</thead><?php $sum = 0; ?>

														@foreach ($cuadroMedicamentos2 as $key=>$cuadro2)
														<tr><?php $sum += $cuadro2->cme_cant; ?>
															<td>{{ ++$key }}</td>
															<td>{{$cuadro2->cme_nombre}}</td>
															<td>{{$cuadro2->cme_cant}}</td>
														</tr>
														@endforeach
													</table>
													<h3 class="panel-title"><b>Total Medicamentos: </b> {{$sum}}
												</div>
												{{$cuadroMedicamentos2->render()}}

											</div>	
										</div>
										</div>

									</div>
  								</fieldset>

  							</div>
							</form>

							</div>

						</center>
					</div>

				@endif
				
			@endif
		</div>

	</div>


@endsection