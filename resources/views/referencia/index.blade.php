@extends ('layouts.vistaForm')


@section ('contenido')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center><h3 style="color:lightcoral">Referencias Médicas</h3></center>
			

			<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Parámetros</h3>
  				</div>

  				<div class="panel-body">
  					<h5><b>Descripción:</b></h5>
					<h5>Muestra las referencias médicas más comunes entre las fechas especificadas.</h5>
			
					<!-- Mostrar mensajes hasta dar click -->
					@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
						@if ( $valFecha == '3')
							<div class="alert alert-danger" role="alert">Debe ingresar las dos fechas</div>
						@endif	

						@if ( $valFecha == '1')
							<div class="alert alert-danger" role="alert">La Fecha de Inicio debe ser menor que la Fecha Fin</div>
						@endif		
					@endif	

					<b>Parametros:</b>
					@include('referencia.datepicker')
				</div>
			</div>


			@if (isset($_GET['mostrar']) && $_GET['mostrar'] == 'Ok')
@if ( $valFecha == '2')
			<div class="panel panel-primary">

  				 
				<div class="panel-heading">
    				<h3 class="panel-title">
    				<b> Entre las fechas </b>{{$fecha1}} 
    				<b> y </b> {{$fecha2}}
    				</h3>
  				</div>
  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										<th>Caso de Referencia</th>
										<th>Cantidad</th>
										
										
									</thead>
									<?php $sum = 0; ?>
									@foreach ($referencias as $morbi)
										<tr><?php $sum += $morbi->ref_cant; ?>
										<td>{{$morbi->ref_causa}}</td>
											<td>{{$morbi->ref_cant}}</td>
											
											
										</tr>
									@endforeach

								</table>
<h3 class="panel-title"><b>Total Referencias: </b> {{$sum}}
							</div>
							{{$referencias->render()}}
						</div>	
					</div>
				</div>
			</div>
@endif
			@endif

		</div>
	</div>


	@if ( $valFecha == '2')
	<div class="form-group">
      	<div class="col-md-offset-10">
			<a class="btn btn-danger" align="right" href="#">Imprimir</a>
		</div>
	</div>
	@endif

@endsection